
var worldmenu = new Vue({
    el: '.container-fluid.py-0.px-0.my-0.mx-0.worldmenu',
    data: {
        active: true,
        menuid: 1,
        dialog: 0,
        totrans: null,
        aftertrans: null,
        firstnameNew: null,
        lastnameNew: null,
        submenuid: 1,
        payactive:false,
        stats:["Alexander_Pisosochkin",//0
        "ell",//1
        "21",//2
        "Дальнобойщик",//3
        "MerryWeather Security",//4
        "1",//5
        "12123453",//6
        "2274",//7
        "1235121",//8
        "11",//9
        "43",//10
        "1872",//11
        "48",//12
        "13",//13
        "3",//14
        "Мужской",//15
        "2",//16
        "2",//17
        "2",//18
        "23",//19
        "13",//20
        "2323",//21
        "682",//22
        "23",//23
        "kekSh1eld@yandex.ua",//24
        "true",//25
        "Bronze VIP", //26,
        "18", //27
        ],
    },
    methods: {
        range_sort: function () {
            // `this` указывает на экземпляр vm
            m = [];
                $.each(this.data.range, function(item, i, arr) {
                    m.push({id:item,Item6:i.Item6});
                  });
            return m.sort(function (d1, d2) { return (d1.Item6 < d2.Item6) ? 1 : -1; });
          },
          man_sort: function(){
            return this.data.man.sort(function (d1, d2) { return (d1.online < d2.online) ? 1 : -1; });

          },
        closeworldmenuMenu: function() {
            mp.trigger("closeworldmenu");
        },
        liveworld: function(status) {
            mp.trigger("liveworld", status, true);
        },
        show: function (level, currentjob,data) {
            this.level = level;
            this.jobid = currentjob;
            this.active = true;
            this.datajobs = data;
        },
        hide: function () {
            this.active = false;
        },

        buy: function(id){
            let data = null;
            switch(id){
                case 1:
                data = this.fname+"_"+this.lname;
                break;
                case 2:
                data = this.totrans;
                break;
                default:
                break;
            }
            mp.trigger("donbuy", id, data);
        },
        onInputTrans: function(){
            if(!this.check(this.totrans)){
                this.totrans = null;
                this.aftertrans = null;
            } else {
                if(Number(this.totrans) < 0) this.totrans = 0;
                this.aftertrans = Number(this.totrans) * 100;
            }
        },
        check: function(str) {
            return (/[^a-zA-Z]/g.test(str));
        },
        opendonate: function() {
            mp.trigger("opendonatemenu");
        },
        selectbutton: function (id, step) {
            console.log(id);
            if (typeof this.data.man[id].button == "undefined" || this.data.man[id].button=='')
                this.data.man[id].button = 1;
            if(step==1 && this.data.man[id].button == 3 )
                this.data.man[id].button = 1;
            else if(step==-1 && this.data.man[id].button == 1 )
                this.data.man[id].button = 3;
            else  this.data.man[id].button =  this.data.man[id].button + step;

        },
        showVehc: function(id){
            this.dialog = 1;
            this.selectVehc = id;
        },
        showRange: function(id){
            this.submenuid = 6
            this.selectRange = id;
        },
        deleteRange: function(id){

            mp.trigger("sendworldmenu","deleteRange",id); 
        },
        btn: function(action,id){
           mp.trigger("sendworldmenu",action,id);
        },
        btnTwo: function(action,id,str){
            mp.trigger("sendworldmenuTwo",action,id,str);
         },
        showworldmenu: function(data){
            this.active = true;
        },
        hideworldmenu: function(){this.active = false;},
        addRange: function(){
            
            var n = Object.keys(this.data.range).length;

            console.log();
            this.data.range[n] = JSON.parse(JSON.stringify(this.data.range[Object.keys(this.data.range)[n-1]]));
            this.data.range[n].Item1 = "";
            this.data.range[n].Item6 = "0";
            this.data.range[n].Item7 = 0;
            this.submenuid = 6
            this.selectRange = n;

        },
        wheel: function(){
            mp.trigger("wheelcall");
        },
        orderRange: function(id,step){
            if(step>0) // вверх
            {
                if (id>0)
                {
                    
                    oldVal = this.data.range[this.range_sort()[id].id].Item6;
                    newVal = this.data.range[this.range_sort()[id-1].id].Item6;
                    keyFind = this.data.range[this.range_sort()[id-1].id].Item1
                    this.data.range[this.range_sort()[id].id].Item6 = newVal;
                    $.each(this.data.range, function(key, item, arr) {
                        if(item.Item1 == keyFind)
                        {
                            item.Item6 = oldVal;
                        }
                      });
                      this.change=1;
                }
            }
            if(step<0) // вверх
            {
                if (id<Object.keys(this.data.range).length-1)
                {
                    
                    oldVal = this.data.range[this.range_sort()[id].id].Item6;
                    newVal = this.data.range[this.range_sort()[id+1].id].Item6;
                    keyFind = this.data.range[this.range_sort()[id+1].id].Item1
                    this.data.range[this.range_sort()[id].id].Item6 = newVal;
                    $.each(this.data.range, function(key, item, arr) {
                        if(item.Item1 == keyFind)
                        {
                            item.Item6 = oldVal;
                        }
                      });
                      this.change=1;
                }
            }
            
        },
        selectJob: function(jobid,select_level) {
            if (typeof select_level == "undefined" ) 
            {
                select_level = 0;
            }
            mp.trigger("selectJob", jobid, select_level);
        }
    }
})